package me.razor.middleage.thread;

import org.bukkit.Bukkit;
import org.bukkit.Location;

import me.razor.middleage.Config;
import me.razor.middleage.Main;
import me.razor.middleage.game.GameState;
import me.razor.middleage.game.Quest;
import me.razor.middleage.game.quests.Steal;

public class WarmupThread extends Thread{
	
	@Override
	public void run() {
		int time = Config.Values.WARMUP_TIME;
		while(GameState.isState(GameState.WARMUP)){
			for(;time > -1; time--){
				if(!Main.allRunningThread.contains(this)) {
					System.out.println("Warmup Thread beendet");
					return;
				}
				if(time % 5 == 0 && !(time == 0)){
					Main.getChat().brSpecific(Config.Strings.WARMUP_TIME_LEFT.replaceAll("%time%",String.valueOf(time)),Main.getGameHandler().all);
				}
				if(time == 0){
					Bukkit.getScheduler().runTask(Main.getPlugin(), new Runnable() {
						
						@Override
						public void run() {
							//Location Regestrieren
							Main.registerListenerForQuest(Main.getPlugin().getServer().getPluginManager());
							
							//Speziel bei Capture
							if(Quest.isQuest(0)) {
								Steal.registerLocations();
								Steal.startRunnable();					
								for(Location loc : Steal.take.values()){
									loc.getBlock().setType(Quest.Config.STEAL.BLOCK_TYPE);
								}
							}
							
							//Msg
							Main.getChat().brSpecific(Config.Strings.GAME_START,Main.getGameHandler().all);
						}
					});
					GameState.setGameState(GameState.INGAME);
				}
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
				
				}
			}
		}
	}
}
