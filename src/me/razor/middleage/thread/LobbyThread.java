package me.razor.middleage.thread;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.lynxplay.api.Useful;

import me.razor.middleage.Config;
import me.razor.middleage.Main;
import me.razor.middleage.game.GameState;
import me.razor.middleage.game.Quest;
import me.razor.middleage.game.Team;
import me.razor.middleage.game.quests.Steal;

public class LobbyThread extends Thread {

	public static int time = Config.Values.LOBBY_TIME;
	public static boolean worldCreated = false;
	
	@Override
	public void run() {
		Main.getConsole().sendMessage(Main.getChat().getPrefix() + " �2Die Lobby wurde gestartet");
		
		while(GameState.isState(GameState.LOBBY)){
			for(; time >= 0 ; time--){
				if(!Main.allRunningThread.contains(this)) {
					System.out.println("LobbyThread beendet");
					return;
				}
				if(time % 30 == 0 || (time < 30 && time != 0)){
					if(time > 30){
						Main.getChat().brSpecific(Config.Strings.LOBBY_TIME_LEFT.replaceAll("%time%",String.valueOf(time)) , Main.getGameHandler().inGame);
						Useful.sendSound(Sound.CLICK,Main.getGameHandler().all);
					}else{
						if(time == 30){
							if(Main.getGameHandler().all.size() < Config.Values.MIN_PLAYER){
								Main.getChat().brSpecific(Config.Strings.NOT_ENOUGH_PLAYER,Main.getGameHandler().all);
								time = Config.Values.LOBBY_TIME;
								Useful.sendSound(Sound.WITHER_DEATH,Main.getGameHandler().all);
							}
						}
						if(time == 25){
							Main.getVoteHandler().endVote();
							Main.getChat().brSpecific(Config.Strings.VOTING_END.replaceAll("%map%",Main.getVoteHandler().getMap().getName().replaceAll("&","�")),Main.getGameHandler().all);
							Main.getMapHandler().copyMapInt(Main.getVoteHandler().getMap().getFolerName());
							Useful.sendSound(Sound.PISTON_EXTEND,Main.getGameHandler().all);
						}
						if(time % 10 == 0){
							Main.getChat().brSpecific(Config.Strings.LOBBY_TIME_LEFT.replaceAll("%time%",String.valueOf(time)) , Main.getGameHandler().inGame);
							Useful.sendSound(Sound.CLICK,Main.getGameHandler().all);
						}else{
							if(time < 20){
								if(time % 5 == 0){
									if(time == 5) {
										Main.getTeamHandler().setSpawns(Main.getVoteHandler().getMap());
									}
									Main.getChat().brSpecific(Config.Strings.LOBBY_TIME_LEFT.replaceAll("%time%",String.valueOf(time)), Main.getGameHandler().inGame);
									Useful.sendSound(Sound.NOTE_PIANO,Main.getGameHandler().all);
								}
								if(time < 5){
									Main.getChat().brSpecific(Config.Strings.LOBBY_TIME_LEFT.replaceAll("%time%",String.valueOf(time)), Main.getGameHandler().inGame);
									Useful.sendSound(Sound.NOTE_PIANO,Main.getGameHandler().all);
								}
								if(!worldCreated){
									Bukkit.getScheduler().runTaskLater(Main.getPlugin(),new Runnable() {
										
										@Override
										public void run() {
											Main.getMapHandler().loadMap(ChatColor.stripColor(Main.getVoteHandler().getMap().getName().replaceAll("&","�")));
											Main.getChat().brSpecific(Config.Strings.QUEST_CHOOSE.replaceAll("%quest%",Quest.getQuest().name()),Main.getGameHandler().all);
											worldCreated = true;
										}
									},0);
								}
							}
						}
					}
				}
				if(time == 0){
					Useful.sendSound(Sound.BLAZE_HIT,Main.getGameHandler().all);
					//Main.getTeamHandler().smoothAllTeams();
					Bukkit.getScheduler().runTaskLater(Main.getPlugin(),new Runnable() {
						
						@Override
						public void run() {
							Main.getTeamHandler().giveRestTeams();
							for(Team t : Main.getTeamHandler().getAllTeams()){
								for(Player member : t.getPlayers()){
									member.teleport(t.getSpawn());
									Main.getPlayerHandler().readPlayer(Quest.getQuest(),member);
								}
							}
							Steal.updateScoreboard();
						}
					},0);
					GameState.setGameState(GameState.WARMUP);
					Thread warmup = new WarmupThread();
					Main.allRunningThread.add(warmup);
					warmup.start();
				}
				Main.getGameHandler().setLevel(time);
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					Bukkit.shutdown();
				}
			}
		}
	}
	
	
}
