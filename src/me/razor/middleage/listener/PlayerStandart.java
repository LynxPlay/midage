package me.razor.middleage.listener;

import me.razor.middleage.Config;
import me.razor.middleage.Main;
import me.razor.middleage.game.GameState;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerStandart implements Listener{
	
	@EventHandler
	public void onJoin(PlayerJoinEvent e){
		final Player p = e.getPlayer();
		e.setJoinMessage(null);
		if(GameState.isState(GameState.LOBBY)){
			Main.getGameHandler().addPlayerToInGame(p);
			Main.getChat().brSpecific(Config.Strings.PLAYER_JOIN.replaceAll("%player%",p.getName()),Main.getGameHandler().all);
			Bukkit.getScheduler().runTaskLater(Main.getPlugin(),new Runnable() {
				
				@Override
				public void run() {
					if(Config.Locs.LOBBY != null){
						p.teleport(Config.Locs.LOBBY);
					}
					Main.getPlayerHandler().setupLobbyInv(p);
				}
			},3);
			return;
		}
		if(GameState.isState(GameState.INGAME)){
			Main.getGameHandler().addPlayerToSpectator(p);
			Main.getChat().brSpecific(Config.Strings.PLAYER_JOIN.replaceAll("%player%",p.getName()),Main.getGameHandler().spectator);
			return;
		}
	}
	
	@EventHandler
	public void onLeft(PlayerQuitEvent e){
		Player p = e.getPlayer();
		Main.getGameHandler().removePlayerToSpectator(p);
		Main.getGameHandler().removePlayerToInGame(p);
		Main.getChat().brSpecific(Config.Strings.PLAYER_LEAVE.replaceAll("%player%",p.getName()),(Main.getGameHandler().isPlayer(p) ? Main.getGameHandler().all : Main.getGameHandler().spectator));
		if(Main.getGameHandler().all.size() < Config.Values.MIN_PLAYER) {
			
		}
	}
	
	@EventHandler
	public void onConnect(PlayerLoginEvent e){
		if(GameState.isState(GameState.LOBBY)){
			if(Main.getGameHandler().all.size() == Config.Values.MAX_PLAYER){
				e.disallow(Result.KICK_OTHER,Main.getChat().getPrefix() +"\n" + Config.Strings.SERVER_FULL);
				return;
			}
		}
		if(GameState.isState(GameState.WARMUP)){
			e.disallow(Result.KICK_OTHER,Main.getChat().getPrefix() +"\n" + Config.Strings.KICK_WARMUP);
			return;
		}
		if(GameState.isState(GameState.RESET)){
			e.disallow(Result.KICK_OTHER,Main.getChat().getPrefix() +"\n" +Config.Strings.KICK_RESET);
			return;
		}
	}
	
	@EventHandler
	public void onInteract(PlayerInteractEvent e){
		if(!GameState.isState(GameState.LOBBY)){
			return;
		}
		if(!Main.getGameHandler().isInGame(e.getPlayer())){
			return;
		}
		if(!e.getAction().equals(Action.RIGHT_CLICK_BLOCK) && !e.getAction().equals(Action.RIGHT_CLICK_AIR)){
			return;
		}
		if(e.getItem() == null || e.getItem().getType() == Material.AIR || e.getItem().getItemMeta() == null || e.getItem().getItemMeta().getDisplayName() == null){
			return;
		}
		if(e.getItem().getItemMeta().getDisplayName().equalsIgnoreCase(Config.Strings.TEAM_ITEM)){
			e.getPlayer().openInventory(Main.getPlayerHandler().getTeamInventory(0));
		}else
		if(e.getItem().getItemMeta().getDisplayName().equalsIgnoreCase(Config.Strings.VOTE_ITEM)){
			e.getPlayer().openInventory(Main.getPlayerHandler().getVoteInv());
		}
	}
	
}
