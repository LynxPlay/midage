package me.razor.middleage.listener;

import me.razor.middleage.Config;
import me.razor.middleage.Main;
import me.razor.middleage.game.GameState;
import me.razor.middleage.game.Map;
import me.razor.middleage.game.Team;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.lynxplay.api.Useful;

public class InventoryListener{

	public static class LobbyInv implements Listener{
		
		@EventHandler
		public void onLobbyInv(InventoryClickEvent e){
			if(!GameState.isState(GameState.LOBBY)){
				return;
			}
			if(!e.getInventory().getName().equalsIgnoreCase(Config.Strings.TEAM_INV) && !e.getInventory().getName().equalsIgnoreCase(Config.Strings.VOTE_INV)){
				return;
			}
			if(!(e.getWhoClicked() instanceof Player)){
				return;
			}
			Player p = (Player) e.getWhoClicked();
			ItemStack item = e.getCurrentItem();
			if(item == null || item.getType() == Material.AIR || item.getItemMeta() == null || item.getItemMeta().getDisplayName() == null){
				return;
			}
			e.setCancelled(true);
			if(e.getInventory().getName().equalsIgnoreCase(Config.Strings.TEAM_INV)){
				Team toJoin = Main.getTeamHandler().getTeam(ChatColor.stripColor(item.getItemMeta().getDisplayName()));
				if(toJoin == null){
					Main.getChat().send(Config.Strings.ERROR, p);
					return;
				}
				Main.getTeamHandler().addPlayerToTeam(p,toJoin);
				Main.getChat().send(Config.Strings.TEAM_JOIN.replaceAll("%team%",toJoin.getName()), p);
				p.closeInventory();
				return;
			}
			if(e.getInventory().getName().equalsIgnoreCase(Config.Strings.VOTE_INV)){
				e.setCancelled(true);
				String mapName = item.getItemMeta().getDisplayName();
				mapName = ChatColor.translateAlternateColorCodes('�',mapName);
				mapName = ChatColor.stripColor(mapName);
				Map toVote = Main.getMapHandler().getMap(mapName);
				if(toVote == null){
					p.closeInventory();
					Main.getChat().send(Config.Strings.ERROR, p);
					return;
				}
				if(Main.getVoteHandler().hasVoted(p)){
					Main.getChat().send(Config.Strings.HAS_VOTED,p);
					p.closeInventory();
					return;
				}
				Main.getVoteHandler().vote(toVote);
				Main.getChat().send(Config.Strings.PLAYER_VOTED.replaceAll("%map%",toVote.getName().replaceAll("&","�")), p);
				Main.getVoteHandler().player_voted.add(p.getUniqueId());
				Main.getVoteHandler().updateScoreboard();
				p.closeInventory();
				Useful.sendSound(Sound.ANVIL_BREAK, p);
			}
			return;
		}
		
	}
	
}
