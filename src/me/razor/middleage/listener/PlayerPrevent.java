package me.razor.middleage.listener;

import me.razor.middleage.Main;
import me.razor.middleage.game.GameState;

import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.lynxplay.api.Useful;

public class PlayerPrevent implements Listener {

	@EventHandler
	public void onLooseFood(FoodLevelChangeEvent e){
		if(!(e.getEntity() instanceof Player)){
			return;
		}
		Player p = (Player) e.getEntity();
		if(GameState.isState(GameState.INGAME)){
			return;
		}
		if(!Main.getGameHandler().isPlayer(p)){
			return;
		}
		e.setCancelled(true);
		p.setFoodLevel(20);
	}

	@EventHandler
	public void onBlockBreake(BlockBreakEvent e){
		Player p = e.getPlayer();
		if(!Main.getGameHandler().isPlayer(p)){
			return;
		}
		if(!GameState.isState(GameState.LOBBY) && !GameState.isState(GameState.WARMUP)){
			return;
		}
		if(p.hasPermission("middleage.admin")){
			return;
		}
		e.setCancelled(true);
		Useful.sendSound(Sound.GHAST_SCREAM,p);
	}
	
	@EventHandler
	public void onPlace(BlockPlaceEvent e){
		Player p = e.getPlayer();
		if(!Main.getGameHandler().isPlayer(p)){
			return;
		}
		if(!GameState.isState(GameState.LOBBY) && !GameState.isState(GameState.WARMUP)){
			return;
		}
		if(p.hasPermission("middleage.admin")){
			return;
		}
		e.setCancelled(true);
		Useful.sendSound(Sound.IRONGOLEM_HIT,p);
	}
	
	@EventHandler
	public void onDrop(PlayerDropItemEvent e){
		Player p = e.getPlayer();
		if(!GameState.isState(GameState.LOBBY)){
			return;
		}
		if(!Main.getGameHandler().isPlayer(p)){
			return;
		}
		if(p.hasPermission("middleage.admin")){
			return;
		}
		e.setCancelled(true);
		Useful.sendSound(Sound.BAT_DEATH,p);
	}
	
	 @EventHandler
	 public void onCollect(PlayerPickupItemEvent e){
		 Player p = e.getPlayer();
			if(!GameState.isState(GameState.LOBBY)){
				return;
			}
			if(!Main.getGameHandler().isPlayer(p)){
				return;
			}
			if(p.hasPermission("middleage.admin")){
				return;
			}
			e.setCancelled(true);
			e.getItem().remove();
			Useful.sendSound(Sound.ANVIL_LAND,p);
	 }
	
	@EventHandler
	public void onDamage(EntityDamageEvent e){
		if(!(e.getEntity() instanceof Player)){
			return;
		}
		if(!GameState.isState(GameState.LOBBY) && !GameState.isState(GameState.WARMUP)){
			return;
		}
		Player p = (Player) e.getEntity();
		if(!Main.getGameHandler().isPlayer(p)){
			return;
		}
		e.setCancelled(true);
	}
}
