package me.razor.middleage.listener;

import me.razor.middleage.Config;
import me.razor.middleage.Main;
import me.razor.middleage.game.GameState;
import me.razor.middleage.game.Quest;
import me.razor.middleage.game.Team;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerRespawnEvent;

public class GameListener implements Listener{

	@EventHandler
	public void onDeath(PlayerDeathEvent e){
		if(!GameState.isState(GameState.INGAME)){
			return;
		}
		final Player p = e.getEntity();
		e.getDrops().clear();
		e.setDeathMessage(null);
		Main.getChat().brSpecific((p.getKiller() == null)
				? Config.Strings.PLAYER_DEATH.replaceAll("&","�").replaceAll("%player%",p.getName())
						: Config.Strings.PLAYER_DEATH_KILLER.replaceAll("&","�").replaceAll("%player%",p.getName()).replaceAll("%killer%",p.getKiller().getName()),Main.getGameHandler().all);
	}
	
	@EventHandler
	public void onRespawn(PlayerRespawnEvent e){
		if(!GameState.isState(GameState.INGAME)){
			return;
		}
		final Player p = e.getPlayer();
		Team team = Main.getTeamHandler().getPlayerTeam(p);
		if(team == null){
			return;
		}
		e.setRespawnLocation(team.getSpawn());
		Main.getPlayerHandler().readPlayer(Quest.getQuest(), p);
	}
	
}
