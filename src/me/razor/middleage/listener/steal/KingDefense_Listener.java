package me.razor.middleage.listener.steal;

import me.razor.middleage.game.GameState;
import me.razor.middleage.game.Quest;
import me.razor.middleage.game.quests.KingDefense;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

public class KingDefense_Listener implements Listener{
	
	@EventHandler
	public void onDeath(PlayerDeathEvent e) {
		if(!Quest.isQuest(1)) {
			return;
		}
		if(!GameState.isState(GameState.INGAME)) {
			return;
		}
		Player p = e.getEntity();
		e.getDrops().clear();
		if(!KingDefense.isKing(p)) {
			return;
		}
	}

}
