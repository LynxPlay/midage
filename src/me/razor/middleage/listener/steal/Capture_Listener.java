package me.razor.middleage.listener.steal;

import me.razor.middleage.Main;
import me.razor.middleage.Utils;
import me.razor.middleage.game.GameState;
import me.razor.middleage.game.Quest;
import me.razor.middleage.game.Team;
import me.razor.middleage.game.quests.Steal;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class Capture_Listener implements Listener{
	
	@EventHandler
	public void onLooseFood(FoodLevelChangeEvent e){
		if(!(e.getEntity() instanceof Player)){
			return;
		}
		Player p = (Player) e.getEntity();
		if(!GameState.isState(GameState.INGAME)){
			return;
		}
		if(!Main.getGameHandler().isPlayer(p)){
			return;
		}
		e.setCancelled(true);
		p.setFoodLevel(20);
	}
	
	@EventHandler
	public void onBlockBreake(BlockBreakEvent e){
		Player p = e.getPlayer();
		if(!GameState.isState(GameState.INGAME)){
			return;
		}
		if(!Main.getGameHandler().isInGame(p)){
			return;
		}
		if(!Quest.isQuest(0)){
			return;
		}
		if(!(e.getBlock().getType() == Quest.Config.STEAL.BLOCK_TYPE)){
			e.setCancelled(true);
			return;
		}
		e.setCancelled(true);
		Team t = null;
		try {
			t = (Team) Utils.getKey(Steal.take,e.getBlock().getLocation());
		} catch (Exception e1) {
			return;
		}
		if(t == Main.getTeamHandler().getPlayerTeam(p)) {
			p.setVelocity(p.getLocation().getDirection().setY(0).multiply(-5D));
			return;
		}
		if(!Steal.getTakeLocsFor(t).equals(e.getBlock().getLocation())) {
			return;
		}
		if(!Steal.stealBlock(t, p)) {
			Main.getChat().send(Quest.Config.STEAL.TWO_BLOCKS, p);
			return;
		}{
			e.getBlock().setType(Material.AIR);
		}
	}
	
	@EventHandler
	public void onBlockPlace(BlockPlaceEvent e){
		Player p = e.getPlayer();
		if(!GameState.isState(GameState.INGAME)){
			return;
		}
		if(!Main.getGameHandler().isInGame(p)){
			return;
		}
		if(!Quest.isQuest(0)){
			return;
		}
		if(!(e.getBlock().getType() == Quest.Config.STEAL.BLOCK_TYPE)){
			e.setCancelled(true);
			return;
		}
		Team t = Main.getTeamHandler().getPlayerTeam(p);
		if(t == null){
			return;
		}
		if(!Steal.getBringLoc(t).equals(e.getBlock().getLocation())) {
			e.setCancelled(true);
			return;
		}
		if(!Steal.placeBlock(t,p)) {
			Main.getChat().send(Quest.Config.STEAL.NEED_OWN_BLOCK, p);
			e.setCancelled(true);
			return;
		}
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onHit(EntityDamageByEntityEvent e) {
		if(!(e.getEntity() instanceof Player)) {
			return;
		}
		Player r = (Player) e.getEntity();
		if(e.getDamager() instanceof Projectile) {
			Projectile pr = (Projectile) e.getDamager();
			if(pr.getShooter() == null || !(pr.getShooter() instanceof Player)) {
				return;
			}
			Player p = (Player) pr.getShooter();
			if(Main.getTeamHandler().hasTeam(p) && Main.getTeamHandler().hasTeam(r)) {
				if(Main.getTeamHandler().getPlayerTeam(p) == Main.getTeamHandler().getPlayerTeam(r)) {
					e.setCancelled(true);
					return;
				}
			}
		}else if(e.getDamager() instanceof Player){
			Player p = (Player) e.getDamager();
			if(Main.getTeamHandler().hasTeam(p) && Main.getTeamHandler().hasTeam(r)) {
				if(Main.getTeamHandler().getPlayerTeam(p) == Main.getTeamHandler().getPlayerTeam(r)) {
					e.setCancelled(true);
					return;
				}
			}
		}
	}
	
	@EventHandler
	public void onLeft(PlayerQuitEvent e) {
		if(Steal.hasFlag.containsKey(e.getPlayer())) {
			Steal.bringBlockBack(Steal.hasFlag.get(e.getPlayer()));
		}
	}
	@EventHandler
	public void onDead(PlayerDeathEvent e) {
		if(Steal.hasFlag.containsKey(e.getEntity())) {
			Steal.bringBlockBack(Steal.hasFlag.get(e.getEntity()));
		}
		if(!(e.getEntity().getKiller() == null)) {
			e.getEntity().getKiller().addPotionEffect(new PotionEffect(PotionEffectType.SPEED,20*10,1));
		}
	}

}
