package me.razor.middleage;

import java.util.HashMap;

import net.minecraft.server.v1_8_R1.ChatSerializer;
import net.minecraft.server.v1_8_R1.IChatBaseComponent;
import net.minecraft.server.v1_8_R1.PacketPlayOutChat;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.craftbukkit.v1_8_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

public class Utils {
	
	public static Object getKey(HashMap<? extends Object,? extends Object> a,Object val){
		for(Object o : a.keySet()){
			if(a.get(o).equals(val)){
				return o;
			}
		}
		return null;
	}
	public static void sendAnnouncement(Player p, String msg) {
	    String s = ChatColor.translateAlternateColorCodes('&', msg);
	    
	    IChatBaseComponent icbc = ChatSerializer.a("{\"text\": \"" + setPlaceholders(p, s) + "\"}");
	    PacketPlayOutChat bar = new PacketPlayOutChat(icbc, (byte)2);
	    ((CraftPlayer)p).getHandle().playerConnection.sendPacket(bar);
	}
	
	@SuppressWarnings("deprecation")
	private static String setPlaceholders(Player p, String s) {
	    int online = Bukkit.getServer().getOnlinePlayers().length;
	    s = s.replace("%player%", p.getName())
	      .replace("%online%",String.valueOf(online))
	      .replace("%xp%", String.valueOf(p.getLevel())
	      .replace("%gamemode%", p.getGameMode().name()));
	    return s;
	}
}
