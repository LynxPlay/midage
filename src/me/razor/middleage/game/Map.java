package me.razor.middleage.game;

import org.bukkit.Material;

public class Map {

	private String name = "";
	private String foldername = "";
	private Material item = null;
	
	public Map(String name ,String folername , Material item) {
		this.name = name;
		this.item = item;
		this.foldername = folername;
	}
	
	public String getName(){
		return name;
	}
	public Material getItem(){
		return item;
	}
	public String getFolerName(){
		return foldername;
	}
}
