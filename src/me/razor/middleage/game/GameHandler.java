package me.razor.middleage.game;

import java.util.ArrayList;

import me.razor.middleage.Config;
import me.razor.middleage.Main;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class GameHandler {

	public ArrayList<Player> inGame = new ArrayList<>();
	public ArrayList<Player> spectator = new ArrayList<>();
	public ArrayList<Player> all = new ArrayList<>();
	
	//START
	public void start(){
		inGame = new ArrayList<>();
		spectator = new ArrayList<>();
		all = new ArrayList<>();
	}
	
	/*
	 * Die Ingame 
	 */
	public void addPlayerToInGame(Player toJoin){
		if(!inGame.contains(toJoin)){
			inGame.add(toJoin);
		}
		if(!all.contains(toJoin)){
			all.add(toJoin);
		}
		Main.getVoteHandler().updateScoreboard();
	}
	public void removePlayerToInGame(Player toRemove){
		if(inGame.contains(toRemove)){
			inGame.remove(toRemove);
		}
		if(all.contains(toRemove)){
			all.remove(toRemove);
		}
	}
	public boolean isInGame(Player toCompare){
		return inGame.contains(toCompare);
	}
	
	/*
	 * Spectator und so ein shit
	 */
	public void addPlayerToSpectator(Player toJoin){
		if(!spectator.contains(toJoin)){
			spectator.add(toJoin);
		}
		if(!all.contains(toJoin)){
			all.add(toJoin);
		}
	}
	public void removePlayerToSpectator(Player toRemove){
		if(spectator.contains(toRemove)){
			spectator.remove(toRemove);
		}
		if(all.contains(toRemove)){
			all.remove(toRemove);
		}
	}
	public boolean isSpectator(Player toCompare){
		return spectator.contains(toCompare);
	}
	
	
	/*
	 * 	F�r funktionen 
	 */
	public boolean isPlayer(Player compare){
		return all.contains(compare);
	}
	
	@SuppressWarnings("deprecation")
	public void addAllOnlineToGame(){
		for(Player p : Bukkit.getOnlinePlayers()){
			addPlayerToInGame(p);
			p.teleport(Config.Locs.LOBBY);
			Main.getPlayerHandler().setupLobbyInv(p);
		}
		Bukkit.getScheduler().runTask(Main.getPlugin(), new Runnable() {
			
			@Override
			public void run() {
				Main.getVoteHandler().updateScoreboard();
			}
		});
	}
	public void setLevel(int timeLeft){
		for(Player p : all){
			p.setLevel(timeLeft);
		}
	}
	
	
}
