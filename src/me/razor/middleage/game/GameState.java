package me.razor.middleage.game;

public enum GameState {

	LOBBY,
	WARMUP,
	INGAME,
	RESET;
	
	private static GameState current = null;
	
	public static boolean isState(GameState toCompare){
		return toCompare == current;
	}
	
	public static void setGameState(GameState newState){
		current = newState;
	}
	public static GameState getGameState(){
		return current;
	}
	
}
