package me.razor.middleage.game.quests;

import java.util.HashMap;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.Effect;
import org.bukkit.FireworkEffect;
import org.bukkit.FireworkEffect.Type;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;
import org.lynxplay.api.ConfigHandler;
import org.lynxplay.api.ItemCreater;

import me.razor.middleage.Config;
import me.razor.middleage.Main;
import me.razor.middleage.Utils;
import me.razor.middleage.game.Quest;
import me.razor.middleage.game.Team;

public class Steal {

	public static HashMap<Team,Integer> gold = new HashMap<>();
	public static HashMap<Team, Location> take = new HashMap<>();
	public static HashMap<Team,Location> bring = new HashMap<>();
	public static HashMap<Player,Team> hasFlag = new HashMap<Player, Team>();
	private static BukkitRunnable run = new BukkitRunnable() {
		
		@Override
		public void run() {
			for(Player p : hasFlag.keySet()) {
				Firework f = p.getWorld().spawn(p.getLocation().add(0, 2, 0),Firework.class);
				FireworkMeta fm = f.getFireworkMeta();
				fm.setPower(1);
				if(!Main.getTeamHandler().hasTeam(p)) {
					f.remove();
					continue;
				}
				fm.addEffect(FireworkEffect.builder().withColor(Color.fromRGB(Main.getTeamHandler().getPlayerTeam(p).getColor())).build());
				f.setFireworkMeta(fm);
				PotionEffectType po = PotionEffectType.values()[new Random().nextInt(PotionEffectType.values().length)];
				if(po == null) {
					p.addPotionEffect(new PotionEffect(PotionEffectType.HEALTH_BOOST,20*6,3));
				}else {
					p.addPotionEffect(new PotionEffect(po,20*6,1));
				}
			}
		}
	};
	
	private static BukkitRunnable info = new BukkitRunnable() {
		
		@Override
		public void run() {
			for(Player p : hasFlag.keySet()) {
				Utils.sendAnnouncement(p,Quest.Config.STEAL.HAS_FLAG.replaceAll("%team%",hasFlag.get(p).getName()));
			}
		}
	};
	
	public static boolean stealBlock(Team t , Player p){
		if(hasFlag.containsKey(p)) {
			return false;
		}else {
			Main.getChat().brSpecific(Quest.Config.STEAL.BLOCK_STEAL.replaceAll("%team%",t.getName()),Main.getGameHandler().all);
			p.getInventory().setItem(5,new ItemCreater(Quest.Config.STEAL.BLOCK_TYPE).setName(t.getName()).build());
			hasFlag.put(p, t);
			return true;
		}
	}
	public static void bringBlockBack(Team t) {
		Location b = take.get(t);
		b.getWorld().playEffect(b,Effect.STEP_SOUND,Quest.Config.STEAL.BLOCK_TYPE);
		b.getBlock().setType(Quest.Config.STEAL.BLOCK_TYPE);
		Main.getChat().brSpecific(Quest.Config.STEAL.BLOCK_REBUILD.replaceAll("%team%",t.getName()),Main.getGameHandler().all);
		Firework f = b.getWorld().spawn(b,Firework.class);
		FireworkMeta fm = f.getFireworkMeta();
		fm.setPower(1);
		fm.addEffect(FireworkEffect.builder().withColor(Color.fromRGB(t.getColor())).with(Type.STAR).build());
		f.setFireworkMeta(fm);
		hasFlag.remove(Utils.getKey(hasFlag,t));
	}
	
	public static void startRunnable() {
		run.runTaskTimer(Main.getPlugin(),0,20*5);
		info.runTaskTimer(Main.getPlugin(),0,20*2);
	}
	
	public static void stop() {
		try {
			run.cancel();
			info.cancel();
		} catch (IllegalStateException e) {
			
		}
	}
	
	public static Location getBringLoc(Team t){
		return (bring.containsKey(t) ? bring.get(t) : null);
	}
	
	public static Location getTakeLocsFor(Team t){
		return (take.containsKey(t) ? take.get(t) : null);
	}
	
	public static boolean placeBlock(Team t , Player p){
		if(!bring.containsKey(t)){
			return false;
		}
		if(hasFlag.values().contains(t)) {
			return false;
		}
		Location b = bring.get(t);
		Main.getChat().brSpecific(Quest.Config.STEAL.BLOCK_PLACED.replaceAll("%team%",t.getName()),Main.getGameHandler().all);
		b.getWorld().playEffect(b,Effect.STEP_SOUND,Quest.Config.STEAL.BLOCK_TYPE);
		b.getBlock().setType(Material.AIR);
		if(hasFlag.containsKey(p)) {
			Team blockFrom = hasFlag.get(p);
			take.get(blockFrom).getBlock().setType(Quest.Config.STEAL.BLOCK_TYPE);
		}
		Firework f = b.getWorld().spawn(b,Firework.class);
		FireworkMeta fm = f.getFireworkMeta();
		fm.setPower(0);
		fm.addEffect(FireworkEffect.builder().withColor(Color.fromRGB(t.getColor())).with(Type.STAR).build());
		f.setFireworkMeta(fm);
		if(hasFlag.containsKey(p)) {
			hasFlag.remove(p);
		}
		gold.put(t,(gold.containsKey(t) ? gold.get(t) + 1 : 1));
		if(gold.get(t) >= Quest.Config.STEAL.BLOCK_TO_WIN){
			//TODO Task Restart
		}
		updateScoreboard();
		return true;
	}
	
	public static void registerLocations() {
		FileConfiguration c = YamlConfiguration.loadConfiguration(Main.getFileHandler().getData());
		String path = "maps."+Main.getVoteHandler().getMap().getFolerName()+".teams.";
		for(String s : c.getConfigurationSection(path).getKeys(false)) {
			Team t = Main.getTeamHandler().getTeam(s);
			if(t == null) {
				Main.getConsole().sendMessage("Das Team konnten nicht geladen werden");
				Bukkit.reload();
			}
			Location bring = ConfigHandler.getLocation(path + "." + s + ".0.bring",Main.getFileHandler().getData());
			Location take = ConfigHandler.getLocation(path + "." + s + ".0.take",Main.getFileHandler().getData());
			if(bring == null || take == null) {
				Main.getConsole().sendMessage("Die Block Locations konnten nicht geladen werden");
				Bukkit.reload();
			}
			Steal.take.put(t,take);
			Steal.bring.put(t,bring);
		}
	}
	
	public static void updateScoreboard(){
		Scoreboard s = Bukkit.getScoreboardManager().getNewScoreboard();
		Objective o = s.registerNewObjective("gold","dummy");
		o.setDisplaySlot(DisplaySlot.SIDEBAR);
		o.setDisplayName(Config.Strings.SCOREBOARD_INGAME.replaceAll("%quest%",Quest.getQuest().name()));
		for(int i = 0 ; i < Main.getTeamHandler().getAllTeams().size() ; i++){
			Team t = Main.getTeamHandler().getAllTeams().get(i);
			if(!gold.containsKey(t)){
				gold.put(t, 0);
			}
			int goldInt = gold.get(t);
			Score sc = o.getScore(t.getName());
			sc.setScore(goldInt);
		}
		for(Player p : Main.getGameHandler().all){
			p.setScoreboard(s);
		}
	}
	
}
