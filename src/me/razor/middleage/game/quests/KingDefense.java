package me.razor.middleage.game.quests;

import java.util.HashMap;

import me.razor.middleage.Config;
import me.razor.middleage.Main;
import me.razor.middleage.game.Quest;
import me.razor.middleage.game.Team;

import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;

public class KingDefense {

	public static HashMap<Player,Team> teamsKing = new HashMap<Player, Team>();
	
	public static void chooseKings() {
		for(Team t : Main.getTeamHandler().getAllTeams()) {
			Player newKing = t.getRandomPlayer();
			if(newKing == null) {
				continue;
			}
			teamsKing.put(newKing,t);
		}
	}
	
	public static boolean hasKing(Team t) {
		return teamsKing.containsValue(t);
	}
	
	public static boolean isKing(Player compare) {
		return teamsKing.containsKey(compare);
	}
	
	public static Team getKingsTeam(Player p) {
		return (teamsKing.containsKey(p)) ? teamsKing.get(p) : null;
	}
	
	public static void killKing(Player king) {
		if(teamsKing.containsKey(king)) {
			teamsKing.remove(king);
		}
	}
	
	//UPDATE THE SCOREBOARD
	public static void updateScoreboard() {
		Scoreboard s = Main.getPlugin().getServer().getScoreboardManager().getNewScoreboard();
		Objective o = s.registerNewObjective("king","dummy");
		o.setDisplaySlot(DisplaySlot.SIDEBAR);
		o.setDisplayName(Config.Strings.SCOREBOARD_INGAME.replaceAll("%quest%",Quest.getQuest().name()));
		int list = 1;
		for(int i = 0 ; i < Main.getTeamHandler().getAllTeams().size() ; i++){
			Team t = Main.getTeamHandler().getAllTeams().get(i);
			Score sc = o.getScore(t.getName() + ": "+(teamsKing.containsValue(t) ? "§b✔" : "§4✘"));
			sc.setScore(list++);
		}
	}
	
}
