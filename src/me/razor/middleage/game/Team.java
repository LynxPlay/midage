package me.razor.middleage.game;

import java.util.ArrayList;
import java.util.Random;

import me.razor.middleage.Main;

import org.bukkit.Location;
import org.bukkit.entity.Player;

public class Team {

	private String name;
	private int maxPlayer;
	private short data = -1;
	private int color = 000000;
	private ArrayList<Player> players = new ArrayList<>();
	private Location spawn; 
	
	public Team(String name , int maxPlayers , short data ,int color) {
		this.name = name;
		this.maxPlayer = maxPlayers;
		this.data = data;
		this.color = color;
	}
	public short getData(){
		return (data == -1 || data > 15) ? 0 : data;
	}
	public boolean addPlayer(Player p){
		if(players.size() + 1 > maxPlayer){
			return false;
		}
		if(Main.getTeamHandler().hasTeam(p)){
			Team last = Main.getTeamHandler().getPlayerTeam(p);
			last.removePlayer(p);
		}
		if(!players.contains(p)){
			players.add(p);
		}
		return true;
	}
	public Player getRandomPlayer() {
	     int randomIndex = new Random().nextInt(players.size());
	     return (!players.isEmpty()) ? players.get(randomIndex) : null;
	}
	public void removePlayer(Player p){
		if(players.contains(p)){
			players.remove(p);
		}
	}
	public ArrayList<Player> getPlayers(){
		return players;
	}
	public String getName(){
		return name;
	}
	public void setSpawn(Location spawn){
		this.spawn = spawn;
	}
	public Location getSpawn(){
		return spawn;
	}
	public int getColor() {
		return color;
	}
	
}
