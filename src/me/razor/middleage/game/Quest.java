package me.razor.middleage.game;

import java.util.Random;

import org.bukkit.Material;

public enum Quest {

	CAPTURE (0) , KING_DEFENSE (1) , DESTROY (2) ;
	
	private static Quest current ;
	private int id = -1;
	
	private Quest(int id) {
		this.id = id;
	}

	public static Quest getQuest() {
		return current;
	}

	public int getId() {
		return id;
	}
	
	public static void setQuest(Quest newOne) {
		current = newOne;
	}
	public static void setQuest(int id) {
		for(Quest q : values()) {
			if(q.getId() == id) {
				current = q;
			}
		}
	}
	
	public static boolean isQuest(Quest toCompare){
		return current == toCompare;
	}
	public static boolean isQuest(int id) {
		return current.getId() == id;
	}
	
	public static void setRandomQuest(){
		if(Quest.values().length < 1){
			return;
		}
		int randomIndex = new Random().nextInt(Quest.values().length);
		current = Quest.values()[randomIndex];
	}
	public static Quest getQuest(String name){
		for(Quest q : values()){
			if(q.name().equalsIgnoreCase(name)){
				return q;
			}
		}
		return null;
	}
	
	public static class Config { 
		
		public static class STEAL{
			public static int REGENERATE = 30;
			public static int BLOCK_TO_WIN = 15;
			public static String BLOCK_REBUILD = "";
			public static String BLOCK_PLACED = "";
			public static String BLOCK_STEAL = "";
			public static String TWO_BLOCKS = "";
			public static Material BLOCK_TYPE = Material.AIR;
			public static String NEED_OWN_BLOCK = "";
			public static String HAS_FLAG = "";
		}
		
	}
	
}
