package me.razor.middleage.commands;

import me.razor.middleage.Config;
import me.razor.middleage.Main;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class WorldLoad_CMD implements CommandExecutor{

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String lable,
			String[] args) {
		if(!(sender instanceof Player)){
			if(!(args.length == 2)){
				return true;
			}
			if(args[0].equalsIgnoreCase("load")){
				final String load = args[1];
				if(Bukkit.getWorld(load) == null){
					Main.getMapHandler().copyMapInt(load);
					Bukkit.getScheduler().runTaskLater(Main.getPlugin(), new Runnable() {
						
						@Override
						public void run() {
							Main.getMapHandler().loadMap(load);
							System.out.println("�ADie Welt "+load+" wurde geladen");
						}
					},20*4);
					return true;
				}else{
					System.out.println("�4FEHLER");
					return true;
				}
			}
			if(args[0].equalsIgnoreCase("unload")){
				String load = args[1];
				if(Bukkit.getWorld(load) != null){
					Main.getMapHandler().unloadMap(load);
					System.out.println("�ADie Welt "+load+" wurde unloaded");
					return true;
				}else{
					System.out.println("�4FEHLER");
					return true;
				}
			}
			return true;
		}
		Player p = (Player) sender;
		if(!p.hasPermission("midage.admin")){
			Main.getChat().send(Config.Strings.NO_PERMS, p);
			return true;
		}
		if(cmd.getName().equalsIgnoreCase("worlds")){
			if(!(args.length == 2)){
				Main.getChat().send("�4So benutzt man den Kommand: /worlds <load|unload|tp> <WorldName>", p);
				return true;
			}
			if(args[0].equalsIgnoreCase("list")){
				if(Main.getMapHandler().getLoadedMaps().isEmpty()){
					Main.getChat().send("�BEs ist keine Map geladen", p);
					return true;
				}
				for(World w : Main.getMapHandler().getLoadedMaps()){
					Main.getChat().send(w.getName(), p);
				}
				return true;
			}
			if(args[0].equalsIgnoreCase("load")){
				final String load = args[1];
				if(Bukkit.getWorld(load) == null){
					Main.getMapHandler().copyMapInt(load);
					Bukkit.getScheduler().runTaskLater(Main.getPlugin(), new Runnable() {
						
						@Override
						public void run() {
							Main.getMapHandler().loadMap(load);
							System.out.println("�ADie Welt "+load+" wurde geladen");
						}
					},20*4);
					return true;
				}else{
					System.out.println("�4FEHLER");
					return true;
				}
			}
			if(args[0].equalsIgnoreCase("unload")){
				String load = args[1];
				if(Bukkit.getWorld(load) != null){
					Main.getMapHandler().unloadMap(load);
					Main.getChat().send("�ADie Welt "+load+" ist nicht mehr geladen", p);
					return true;
				}else{
					Main.getChat().send("�4Die Welt "+load+" ist nicht geladen", p);
					return true;
				}
			}
			if(args[0].equalsIgnoreCase("tp")){
				String load = args[1];
				if(Bukkit.getWorld(load) == null){
					Main.getChat().send("�4Die Welt "+load+" ist nicht geladen", p);
					return true;
				}else{
					p.teleport(Bukkit.getWorld(load).getSpawnLocation());
					Main.getChat().send("�BDu wurdest teleportiert", p);
				}
			}
			else{
				Main.getChat().send("�4So benutzt man den Kommand: /worlds <load|unload> <WorldName>", p);
				return true;
			}
		}
		return false;
	}

}
