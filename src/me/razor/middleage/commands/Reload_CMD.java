package me.razor.middleage.commands;

import me.razor.middleage.Config;
import me.razor.middleage.Main;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

public class Reload_CMD implements CommandExecutor{

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String lable,
			String[] args) {
		if(sender instanceof ConsoleCommandSender){
			if(cmd.getName().equalsIgnoreCase("midage")){
				if(args.length == 1){
					if(args[0].equalsIgnoreCase("relaod") || args[0].equalsIgnoreCase("rl")){
						Main.getFileHandler().reload();
						Main.getConsole().sendMessage(Main.getChat().getPrefix() + "�2Das Plugin wurde neu geladen");
						return true;
					}
				}
				return true;
			}
		}
		if(sender instanceof Player){
			Player p = (Player) sender;
			if(cmd.getName().equalsIgnoreCase("midage")){
				if(!p.hasPermission("midage.admin")){
					Main.getChat().send(Config.Strings.NO_PERMS, p);
					return true;
				}
				if(args.length == 1){
					if(args[0].equalsIgnoreCase("relaod") || args[0].equalsIgnoreCase("rl")){
						Main.getFileHandler().reload();
						Main.getChat().send("�2Das Plugin wurde neu geladen", p);
						return true;
					}
					if(args[0].equalsIgnoreCase("mysql")){
						Main.getFileHandler().getFromMySQL();
						Main.getChat().send("�2Das Plugin wurde neu geladen", p);
						return true;
					}
				}
				return true;
			}
		}
		return false;
	}

}
