package me.razor.middleage.commands;

import me.razor.middleage.Config;
import me.razor.middleage.Main;

import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.lynxplay.api.ConfigHandler;

public class SetLobby_CMD implements CommandExecutor{

	@SuppressWarnings("deprecation")
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String lable,
			String[] args) {
		if(!(sender instanceof Player)){
			Main.getConsole().sendMessage(Main.getChat().getPrefix() + "�4Dieser Kommand ist nur f�r Spieler verf�gbar");
			return true;
		}
		Player p = (Player) sender;
		if(!cmd.getName().equals("setlobby")){
			return true;
		}
		if(!p.hasPermission("middleage.admin")){
			Main.getChat().send(Config.Strings.NO_PERMS, p);
			return true;
		}
		ConfigHandler.setLocation("lobby",p.getLocation(),Main.getFileHandler().getData());
		Main.getFileHandler().reload();
		Main.getChat().send("�ADer Lobbyspawn wurde auf diese Postition gesetzt", p);
		p.sendBlockChange(p.getLocation(),Material.WOOL,(byte) 14);
		return true;
	}

	
	
}
