package me.razor.middleage.commands;

import me.razor.middleage.Main;
import me.razor.middleage.game.GameState;
import me.razor.middleage.thread.LobbyThread;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Start_CMD implements CommandExecutor{

	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String lable,
			String[] args) {
		if(!(sender instanceof Player)){
			return true;
		}
		Player p = (Player) sender;
		if(!p.hasPermission("midage.start")){
			return true;
		}
		if(!cmd.getName().equalsIgnoreCase("start")){
			return true;
		}
		if(!GameState.isState(GameState.LOBBY)){
			Main.getChat().send("�4Dieser Kommand ist nur in der Lobby verf�gbar", p);
			return true;
		}
		LobbyThread.time = 29;
		return true;
		
	}

}
