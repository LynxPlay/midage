package me.razor.middleage.commands;

import me.razor.middleage.Config;
import me.razor.middleage.Main;
import me.razor.middleage.game.Team;
import org.bukkit.ChatColor;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.lynxplay.api.ConfigHandler;

public class Steal_CMD implements CommandExecutor{

	@SuppressWarnings("deprecation")
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String lable,
			String[] args) {
		if(!(sender instanceof Player)){
			Main.getConsole().sendMessage("�4Dieser Kommand ist nur fuer Spieler");
			return true;
		}
		Player p = (Player) sender;
		if(cmd.getName().equalsIgnoreCase("steal")){
			if(!p.hasPermission("midage.steal")){
				Main.getChat().send(Config.Strings.NO_PERMS, p);
				return true;
			}
			if(!(args.length == 2)){
				Main.getChat().send("�4/steal <setTake|setBring> <Team>", p);
				return true;
			}
			if(args[0].equalsIgnoreCase("setTake")){
				Team t = Main.getTeamHandler().getTeam(args[1]);
				if(t == null){
					Main.getChat().send("�4Das von dir angegebene Team (�6"+args[1]+"�4) exestiert nicht", p);
					return true;
				}
				String worldName = p.getWorld().getName();
				String teamName = ChatColor.stripColor(args[1]);
				Block tagert = p.getTargetBlock(null,6);
				p.sendBlockChange(tagert.getLocation(),1,(byte) 0);
				ConfigHandler.setLocation("maps."+worldName+".teams."+teamName+".0.take",tagert.getLocation(),Main.getFileHandler().getData());
				Main.getChat().send("�BDie Gold-Spawn des Teams �6"+t.getName()+"�B wurde gesetzt", p);
				return true;
			}
			if(args[0].equalsIgnoreCase("setBring")){
				Team t = Main.getTeamHandler().getTeam(args[1]);
				if(t == null){
					Main.getChat().send("�4Das von dir angegebene Team (�6"+args[1]+"�4) exestiert nicht", p);
					return true;
				}
				String worldName = p.getWorld().getName();
				String teamName = ChatColor.stripColor(args[1]);
				Block tagert = p.getTargetBlock(null,6);
				p.sendBlockChange(tagert.getLocation(),1,(byte) 0);
				ConfigHandler.setLocation("maps."+worldName+".teams."+teamName+".0.bring",tagert.getLocation(),Main.getFileHandler().getData());
				Main.getChat().send("�BDie Location der Gold-AblagePunkt des Teams �6"+t.getName()+"�B wurde gesetzt", p);
				return true;
			}
		}
		return false;
	}
	
}
