package me.razor.middleage.commands;

import me.razor.middleage.Config;
import me.razor.middleage.Main;
import me.razor.middleage.game.GameState;
import me.razor.middleage.game.Team;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class GetTeam_CMD implements CommandExecutor{

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String lable,
			String[] args) {
		if(!(sender instanceof Player)){
			return true;
		}
		Player p = (Player) sender;
		if(!cmd.getName().equalsIgnoreCase("getTeam")){
			return true;
		}
		if(!GameState.isState(GameState.LOBBY)){
			Main.getChat().send("�4Dieser Kommand ist nur in der Lobby verf�gbar", p);
			return true;
		}
		if(!Main.getTeamHandler().hasTeam(p)){
			Main.getChat().send("�7Du hast leider kein Team", p);
			return true;
		}
		Team t = Main.getTeamHandler().getPlayerTeam(p);
		Main.getChat().send(Config.Strings.GET_TEAM.replaceAll("%team%",t.getName()), p);
		return true;
	}

}