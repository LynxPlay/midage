package me.razor.middleage.commands;

import me.razor.middleage.Main;
import me.razor.middleage.game.Quest;
import me.razor.middleage.game.Team;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.lynxplay.api.ConfigHandler;

public class Inventory_CMD implements CommandExecutor{

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String lable,
			String[] args) {
		if(!(sender instanceof Player)){
			Main.getConsole().sendMessage("�4Dieser Kommand ist nur fuer Spieler");
			return true;
		}
		if(!cmd.getName().equalsIgnoreCase("setinv")){
			return true;
		}
		Player p = (Player) sender;
		if(!(args.length == 2)){
			Main.getChat().send("�4Usage: /setinv <KLAUEN|KING_DEFENSE|DESTROY> <Team>", p);
			return true;
		}
		String gamemode = args[0].toUpperCase();
		String teamName = args[1];
		Quest q = Quest.getQuest(gamemode);
		if(q == null){
			Main.getChat().send("�4Der con die angegebende Spielmodi (�6"+gamemode+"�4) exestiert nicht", p);
			return true;
		}
		Team t = Main.getTeamHandler().getTeam(teamName);
		if(t == null){
			Main.getChat().send("�4Das con die angegebende Team (�6"+teamName+"�4) exestiert nicht", p);
			return true;
		}
		ConfigHandler.saveInventory(q.getId() + "."+ChatColor.stripColor(t.getName()) + ".inv",p.getInventory().getContents(),Main.getFileHandler().getTeamFile());
		ConfigHandler.saveInventory(q.getId() + "."+ChatColor.stripColor(t.getName()) + ".armor",p.getInventory().getArmorContents(),Main.getFileHandler().getTeamFile());
		Main.getChat().send("�bDas Inventar f�r das Team "+t.getName()+"�b wurde gespiechert", p);
		return true;
	}
	
}
