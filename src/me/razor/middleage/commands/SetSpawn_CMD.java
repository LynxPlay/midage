package me.razor.middleage.commands;

import me.razor.middleage.Main;
import me.razor.middleage.game.Team;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.lynxplay.api.ConfigHandler;

public class SetSpawn_CMD implements CommandExecutor{

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String lable,
			String[] args) {
		if(!(sender instanceof Player)){
			return true;
		}
		Player p = (Player) sender;
		if(!cmd.getName().equalsIgnoreCase("setspawn")){
			return true;
		}
		if(args.length == 0){
			Main.getChat().send(ChatColor.GRAY+ "/setspawn <TeamName>", p);
			return true;
		}
		if(args.length == 1){
			String worldName = p.getWorld().getName();
			String teamName = args[0];
			Team t = Main.getTeamHandler().getTeam(ChatColor.stripColor(teamName));
			if(t == null){
				Main.getChat().send("�4Das von dir angegebene Team (�6"+teamName+"�4) exestiert nicht", p);
				return true;
			}
			ConfigHandler.setLocation("maps."+worldName+".teams."+teamName+".spawn", p.getLocation(),Main.getFileHandler().getData());
			Main.getChat().send("�2Das Spawn des Teams �6"+teamName+"�2 wurde gesetzt", p);
			return true;
		}else{
			Main.getChat().send(ChatColor.GRAY+ "/setspawn <TeamName>", p);
			return true;
		}
	}

	
	
}
