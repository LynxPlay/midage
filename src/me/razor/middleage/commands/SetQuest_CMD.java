package me.razor.middleage.commands;

import me.razor.middleage.Main;
import me.razor.middleage.game.Quest;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SetQuest_CMD implements CommandExecutor{

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String lable,
			String[] args) {
		if(!(sender instanceof Player)) {
			return true;
		}
		Player p = (Player) sender;
		if(cmd.getName().equalsIgnoreCase("setquest")) {
			if(!sender.hasPermission("midage.admin")) {
				return true;
			}
			if(!(args.length == 1)) {
				Main.getChat().send("�4Usage: /setQuest <QuestType>", p);
				return true;
			}
			Quest q = Quest.getQuest(args[0]);
			if(q == null) {
				Main.getChat().send("�4Die Quest �6"+args[0]+"�4 giebt es nicht", p);
				return true;
			}
			Quest.setQuest(q);
			Main.getChat().send("�BNun wird die Quest �6"+q.name()+" �Bgespielt", p);
			return true;
		}
		return false;
	}
	
}
