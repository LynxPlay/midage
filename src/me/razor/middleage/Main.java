package me.razor.middleage;

import java.util.ArrayList;

import me.razor.middleage.commands.GetTeam_CMD;
import me.razor.middleage.commands.Inventory_CMD;
import me.razor.middleage.commands.Reload_CMD;
import me.razor.middleage.commands.SetLobby_CMD;
import me.razor.middleage.commands.SetQuest_CMD;
import me.razor.middleage.commands.SetSpawn_CMD;
import me.razor.middleage.commands.Start_CMD;
import me.razor.middleage.commands.Steal_CMD;
import me.razor.middleage.commands.WorldLoad_CMD;
import me.razor.middleage.game.GameHandler;
import me.razor.middleage.game.GameState;
import me.razor.middleage.game.Quest;
import me.razor.middleage.game.Team;
import me.razor.middleage.game.quests.Steal;
import me.razor.middleage.handler.FileHandler;
import me.razor.middleage.handler.MapHandler;
import me.razor.middleage.handler.PlayerHandler;
import me.razor.middleage.handler.TeamHandler;
import me.razor.middleage.handler.VoteHandler;
import me.razor.middleage.listener.GameListener;
import me.razor.middleage.listener.InventoryListener;
import me.razor.middleage.listener.PlayerPrevent;
import me.razor.middleage.listener.PlayerStandart;
import me.razor.middleage.listener.steal.Capture_Listener;
import me.razor.middleage.thread.LobbyThread;
import me.razor.mysql.api.MySQL_API;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.lynxplay.api.Chat;

public class Main extends JavaPlugin{

	private static Chat chat = null;
	private static ConsoleCommandSender console = null;
	private static TeamHandler teamHandler = null;
	private static FileHandler fileHandler = null;
	private static PlayerHandler playerhHandler = null;
	private static MapHandler mapHandler = null;
	private static GameHandler gameHandler = null;
	private static VoteHandler voteHandler = null;
	private static MySQL_API mysql = null;
	public static ArrayList<Thread> allRunningThread = new ArrayList<Thread>();
	
	@Override
	public void onEnable() {
		
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				mysql = new MySQL_API("MidAge");
			}
		}).start();
		chat = new Chat();
		console = getServer().getConsoleSender();
		teamHandler = new TeamHandler();
		fileHandler = new FileHandler();
		playerhHandler = new PlayerHandler();
		mapHandler = new MapHandler();
		voteHandler = new VoteHandler();
		gameHandler = new GameHandler();
	
		Main.getFileHandler().createAllFiles();
		Main.getFileHandler().reload();
		Quest.setQuest(0);
		//Quest.setRandomQuest();
		
		registerCommands();
		registerListener(getServer().getPluginManager());
		
		registerTeams();
		
		mapHandler.registerAllMaps();
		mapHandler.chooseMaps();
		
		gameHandler.start();
		GameState.setGameState(GameState.LOBBY);
		
		Bukkit.getScheduler().runTask(Main.getPlugin(), new Runnable() {
			
			@Override
			public void run() {
				gameHandler.addAllOnlineToGame();
			}
		});
		Thread lobby = new LobbyThread();
		allRunningThread.add(lobby);
		lobby.start();
		
		getMapHandler().deleteAllWorlds();
		
		Main.getConsole().sendMessage(Main.getChat().getPrefix() + " �2Das Plugin wurde geladen");
	}
	
	private void registerTeams(){
		FileConfiguration f = YamlConfiguration.loadConfiguration(Main.getFileHandler().getConfig());
		for(String s : f.getConfigurationSection("teams").getKeys(false)){
			String pathToTeam = "teams."+s;
			String color = f.getString(pathToTeam + ".color");
			int rgb = Integer.valueOf(color,16);
			Main.getTeamHandler().addTeam(new Team(f.getString(pathToTeam+".name").replaceAll("&","�"),
					Config.Values.TEAM_SIZE,
					(short) f.getInt(pathToTeam + ".data") ,
					rgb));
			getConsole().sendMessage("Das Team "+f.getString(pathToTeam + ".name").replaceAll("&","�")+"�R wurde registriert ("+(short) f.getInt(pathToTeam + ".data") +"|"+rgb+")");
		}
	}
	
	private void registerListener(PluginManager pm) {
		pm.registerEvents(new PlayerPrevent(), this);
		pm.registerEvents(new PlayerStandart(), this);
		pm.registerEvents(new InventoryListener.LobbyInv(), this);
		pm.registerEvents(new GameListener(), this);
	}

	private void registerCommands() {
		getCommand("setlobby").setExecutor(new SetLobby_CMD());
		getCommand("midage").setExecutor(new Reload_CMD());
		getCommand("setSpawn").setExecutor(new SetSpawn_CMD());
		getCommand("getTeam").setExecutor(new GetTeam_CMD());
		getCommand("start").setExecutor(new Start_CMD());
		getCommand("worlds").setExecutor(new WorldLoad_CMD());
		getCommand("setinv").setExecutor(new Inventory_CMD());
		getCommand("steal").setExecutor(new Steal_CMD());
		getCommand("setquest").setExecutor(new SetQuest_CMD());
	}

	@Override
	public void onDisable() {
		allRunningThread.clear();
		if(Quest.isQuest(0)) {
			Steal.stop();
		}
		GameState.setGameState(GameState.RESET);
		for(World w : getMapHandler().getLoadedMaps()){
			mapHandler.unloadWorld(w);
		}
	}
	
	//Die Standart Getter
	public static Plugin getPlugin(){
		return getPlugin(Main.class);
	}
	public static MySQL_API getMySQL(){
		return mysql;
	}
	public static Chat getChat(){
		return chat;
	}
	public static ConsoleCommandSender getConsole(){
		return console;
	}
	public static TeamHandler getTeamHandler(){
		return teamHandler;
	}
	public static MapHandler getMapHandler(){
		return mapHandler;
	}
	public static PlayerHandler getPlayerHandler(){
		return playerhHandler;
	}
	public static GameHandler getGameHandler(){
		return gameHandler;
	}
	public static FileHandler getFileHandler(){
		return fileHandler;
	}
	public static VoteHandler getVoteHandler(){
		return voteHandler;
	}
	public static void registerListenerForQuest(PluginManager pm) {
		if(Quest.isQuest(0)){
			pm.registerEvents(new Capture_Listener(),getPlugin());
		}
	}
	
	@SuppressWarnings("deprecation")
	public static void restart() {
		allRunningThread.clear();
		for(Player p : Bukkit.getOnlinePlayers()) {
			p.kickPlayer(Config.Strings.KICK_RESET);
		}
		if(Quest.isQuest(0)) {
			Steal.stop();
		}
		GameState.setGameState(GameState.RESET);
		for(World w : getMapHandler().getLoadedMaps()){
			mapHandler.unloadWorld(w);
		}
		Main.getFileHandler().createAllFiles();
		Main.getFileHandler().reload();
		Quest.setQuest(0);
		//Quest.setRandomQuest();
		
		mapHandler.registerAllMaps();
		mapHandler.chooseMaps();
		
		gameHandler.start();
		GameState.setGameState(GameState.LOBBY);
		
		gameHandler.addAllOnlineToGame();
		Thread lobby = new LobbyThread();
		allRunningThread.add(lobby);
		lobby.start();
		
		getMapHandler().deleteAllWorlds();
		
		Main.getConsole().sendMessage(Main.getChat().getPrefix() + " �2---------Neustart--------");
	}
}
