package me.razor.middleage.handler;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import me.razor.middleage.Main;
import me.razor.middleage.game.Map;

import org.apache.commons.io.FileUtils;
import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

public class MapHandler {

	private ArrayList<Map> allMaps = new ArrayList<>();
	private ArrayList<Map> choosenMaps = new ArrayList<>();
	private ArrayList<World> loaded = new ArrayList<>();
	
	//Unloading maps, to rollback maps. Will delete all player builds until last server save
    public synchronized void unloadMap(String mapname){
    	World toRollBack = Bukkit.getWorld(mapname);
    	if(toRollBack == null){
    		return;
    	}
    	for(Player p : toRollBack.getPlayers()){
    		p.kickPlayer(null);
    	}
    	if(loaded.contains(toRollBack)){
    		loaded.remove(toRollBack);
    	}
    	final File worldFolder = toRollBack.getWorldFolder();
        if(unloadWorld(toRollBack)){
           Main.getConsole().sendMessage("�BDie Map wird nun unloaded");
           deleteDirectory(worldFolder);
           Main.getConsole().sendMessage("�BDie Map "+worldFolder.getName()+" wurde gel�scht");
        }else{
            Main.getPlugin().getLogger().severe("DIE MAP " + mapname + " ERGAB EINEN FEHLER");
        }
    }
    public boolean unloadWorld(World w){
    	for(Chunk c : w.getLoadedChunks()) {	 
    		c.unload(); 
    	}
    	return Bukkit.getServer().unloadWorld(w, true);
    }

    public static boolean deleteDirectory(File path) {    	
    	if(path.exists()){
    		File[] files = path.listFiles();
    		for(int i = 0 ; i < files.length ; i++){
    			 if(files[i].isDirectory()){
    				 deleteDirectory(files[i]);
    			 }else{
    				 files[i].delete();
    			 }
    		}
    		return path.delete();
    	}
    	return false;
    	
    }
    
    public void copyMapInt(String mapname) {
    	File dest = new File(System.getProperty("use.dir"),mapname);
		File from = new File(Main.getFileHandler().getWorldsFolder(),mapname);
		if(!from.exists()){
			System.out.println("Datei giebt es nicht");
			return;
		}
		try {
			FileUtils.copyDirectory(new File(Main.getFileHandler().getWorldsFolder(),mapname),dest);
		} catch (IOException e) {
			return;
		}
    }

    //Loading maps (MUST BE CALLED AFTER UNLOAD MAPS TO FINISH THE ROLLBACK PROCESS)
    public synchronized void loadMap(final String mapname){
		Bukkit.getScheduler().runTaskLater(Main.getPlugin(), new Runnable() {
			
			@Override
			public void run() {
				World create = Bukkit.getServer().createWorld(new WorldCreator(mapname));
			    create.setAutoSave(false);
			    if(!loaded.contains(create)){
		        	loaded.add(create);
		        }
			}
		},0);
    }
 
    
    //Maprollback method, because were too lazy to type 2 lines
    public void rollback(String mapname){
        unloadMap(mapname);
        loadMap(mapname);
    }
    
    public void registerAllMaps(){
		FileConfiguration c = YamlConfiguration.loadConfiguration(Main.getFileHandler().getData());
		if(!c.contains("maps")){
			return;
		}
		for(String world : c.getConfigurationSection("maps").getKeys(false)){
			for(File f : Main.getFileHandler().getWorldsFolder().listFiles()){
				if(f.getName().equalsIgnoreCase(world)){
					try {
						allMaps.add(new Map(c.getString("maps."+world+".name"), world, Material.valueOf(c.getString("maps."+world+".item"))));
					} catch (Exception e) {
						e.printStackTrace();
						Main.getConsole().sendMessage("�4Die Map "+world + " konnt nicht geladen werden");
						Main.getConsole().sendMessage("�4Gruende dafuer:");
						Main.getConsole().sendMessage("�41.: In der data.yml fehlen Daten");
						Main.getConsole().sendMessage("�42.: Die data.yml hat nicht die richtige formatierung");
						Main.getConsole().sendMessage("�43.: Der WorldFolder f�r diese Map konnt nicht gefunden werden");
						Main.getConsole().sendMessage("�4.:  (Der Folder muss dem Namen in der Data entsprechen (Der Pfad zu name und item))");
						continue;
					}
					Main.getConsole().sendMessage("�ADie Map "+world + " wurde gefunden");
				}
			}
		}
		return;
	}
	
	public void chooseMaps(){
		ArrayList<Integer> randomNubers = new ArrayList<>();
		for(int i = 0 ; (i < allMaps.size() && i < 3); i++){
			int r = new Random().nextInt(allMaps.size());
			while(randomNubers.contains(r)){
				r = new Random().nextInt(allMaps.size());
			}
			randomNubers.add(r);
		}
		Main.getConsole().sendMessage("�BEine Liste der Maps zur Auswahl");
		for(int r : randomNubers){
			choosenMaps.add(allMaps.get(r));
			Main.getConsole().sendMessage("�B"+allMaps.get(r).getName());
		}
	}
	
	public ArrayList<Map> getMaps(){
		return choosenMaps;
	}
	public void stopMaps(){
		
	}
	public Map getMap(String mapName){
		for(Map m : allMaps){
			if(m.getFolerName().equalsIgnoreCase(mapName)){
				return m;
			}
		}
		return null;
	}
	public ArrayList<World> getLoadedMaps(){
		return loaded;
	}
	public void deleteAllWorlds() {
		for(File f : Main.getFileHandler().getWorldsFolder().listFiles()){
			for(File v : Bukkit.getWorld("world").getWorldFolder().getParentFile().listFiles()){
				if(f.getName().equalsIgnoreCase(v.getName())){
					try {
						unloadMap(v.getName());
						FileUtils.deleteDirectory(v);
					} catch (IOException e) {
						
					}
					System.out.println("DATEI "+v.getName()+" WURDE GEL�CHT");
				}
			}
		}
	}
}
