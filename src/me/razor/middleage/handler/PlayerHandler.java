package me.razor.middleage.handler;

import java.util.ArrayList;

import me.razor.middleage.Config;
import me.razor.middleage.Main;
import me.razor.middleage.game.Map;
import me.razor.middleage.game.Quest;
import me.razor.middleage.game.Team;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.lynxplay.api.ConfigHandler;
import org.lynxplay.api.ItemCreater;

public class PlayerHandler {

	public ArrayList<ItemStack> getTeamItems(){
		ArrayList<ItemStack> all = new ArrayList<>();
		for(Team t : Main.getTeamHandler().getAllTeams()){
			all.add(new ItemCreater(Material.WOOL).setName(t.getName().replaceAll("&","�")).setData(t.getData()).build());
		}
		return all;
	}
	
	public Inventory getTeamInventory(int page){
		int teamSize = getTeamItems().size();
		Inventory i = Bukkit.createInventory(null,
				(teamSize > 56) ? 56 : (teamSize) * 9,
						Config.Strings.TEAM_INV.replaceAll("&","�"));
		i.setItem(i.getSize() - 1,new ItemCreater(Material.NAME_TAG).setName("�6Seite").setAmount(page + 1).build());
		int indexReal = page * 56;
		int indexMax = indexReal + 55;
		for(int is = indexReal;is < ((indexMax > teamSize) ? teamSize : indexMax); is++){
			i.setItem(is,getTeamItems().get(is));
		}
		return i;
	}
	
	public void setupLobbyInv(Player p){
		p.getInventory().clear();
		p.getInventory().setArmorContents(null);
		p.getInventory().setItem(2,new ItemCreater(Material.NETHER_STAR).setName(Config.Strings.TEAM_ITEM).build());
		p.getInventory().setItem(6,new ItemCreater(Material.PAPER).setName(Config.Strings.VOTE_ITEM).build());
	}
	
	public Inventory getVoteInv(){
		Inventory i = Bukkit.createInventory(null,9, Config.Strings.VOTE_INV);
		ArrayList<Map> maps = Main.getMapHandler().getMaps();
		int index = 0;
		for(int is = 1 ; (is<8 && index<maps.size()) ;is=is+3){
			try {
				Map map = maps.get(index);
				i.setItem(is,new ItemCreater(map.getItem()).setName(map.getName().replaceAll("&","�")).build());
				index++;
			} catch (Exception e) {
				System.out.println("Fehler");
				return i;
			}
		}
		return i;
	}
	
	public void readPlayer(Quest q , Player p) {
		p.getInventory().clear();
		p.getInventory().setArmorContents(null);
		if(!Main.getTeamHandler().hasTeam(p)) {
			return;
		}
		Team t = Main.getTeamHandler().getPlayerTeam(p);
		ItemStack[] inv = ConfigHandler.getInventoy(q.getId() + "."+ChatColor.stripColor(t.getName()) + ".inv", Main.getFileHandler().getTeamFile());
		ItemStack[] armor = ConfigHandler.getInventoy(q.getId() + "."+ChatColor.stripColor(t.getName()) + ".armor", Main.getFileHandler().getTeamFile());
		p.getInventory().setContents(inv);
		p.getInventory().setArmorContents(armor);
		p.updateInventory();
	}
	
}
