package me.razor.middleage.handler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import me.razor.middleage.Config;
import me.razor.middleage.Main;
import me.razor.middleage.game.Map;
import me.razor.middleage.game.Team;
import me.razor.middleage.thread.LobbyThread;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.lynxplay.api.ConfigHandler;

public class TeamHandler {

	private ArrayList<Team> allTeams = new ArrayList<>();
	public HashMap<Player, Team> playerTeam = new HashMap<>();
	
	
	public void addTeam(Team t){
		if(!allTeams.contains(t)){
			allTeams.add(t);
		}
	}
	public void removeAllTeams(){
		allTeams.clear();
	}
	public boolean addPlayerToTeam(Player p ,Team t){
		if(t == null){
			return true;
		}
		if(playerTeam.containsKey(p)){
			playerTeam.get(p).removePlayer(p);
			playerTeam.put(p,t);
			t.addPlayer(p);
			return false;
		}else{
			playerTeam.put(p,t);
			t.addPlayer(p);
			return true;
		}
	}
	public void giveRestTeams(){
		for(Player p : Main.getGameHandler().all){
			if(!Main.getTeamHandler().hasTeam(p)){
				Team teamLowest = null;
				for(Team t : Main.getTeamHandler().getAllTeams()){
					if(teamLowest == null || teamLowest.getPlayers().size() > t.getPlayers().size()){
						teamLowest = t;
					}
				}
				if(teamLowest == null){
					Main.getTeamHandler().addPlayerToTeam(p,Main.getTeamHandler().getAllTeams().get(new Random().nextInt(Main.getTeamHandler().getAllTeams().size())));
				}else{
					Main.getTeamHandler().addPlayerToTeam(p,teamLowest);
				}
			}
		}
	}
	public Team getPlayerTeam(Player p){
		return (playerTeam.containsKey(p)) ? playerTeam.get(p) : null;
	}
	public boolean hasTeam(Player p){
		return playerTeam.containsKey(p);
	}
	public void setSpawns(Map m){
		FileConfiguration c = YamlConfiguration.loadConfiguration(Main.getFileHandler().getData());
		if(!c.contains("maps."+m.getFolerName()+".teams")){
			Main.getConsole().sendMessage("�4Den Teams wurden keine Spawnspoints gegeben");
			Main.getConsole().sendMessage("�4/setspawn <Teamname> in der Map "+Main.getVoteHandler().getMap().getName());
			LobbyThread.time = Config.Values.LOBBY_TIME;
			Main.getGameHandler().addAllOnlineToGame();
			return;
		}
		for(String team : c.getConfigurationSection("maps."+m.getFolerName()+".teams").getKeys(false)){
			Team cur = Main.getTeamHandler().getTeam(team);
			Location spawn = ConfigHandler.getLocation("maps."+m.getFolerName()+".teams."+ChatColor.stripColor(cur.getName())+".spawn",Main.getFileHandler().getData());
			if(spawn == null){
				Main.getConsole().sendMessage("�4Das Team "+team+" konnte nicht geladen werden");
				continue;
			}
			cur.setSpawn(spawn);
		}
	}
	public ArrayList<Team> getAllTeams(){
		return allTeams;
	}
	public Team getTeam(String name){
		for(Team t : allTeams){
			if(ChatColor.stripColor(t.getName()).equalsIgnoreCase(name)){
				return t;
			}
		}
		return null;
	}
}
