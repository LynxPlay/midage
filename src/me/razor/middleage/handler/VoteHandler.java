package me.razor.middleage.handler;

import java.util.ArrayList;
import java.util.Map.Entry;
import java.util.HashMap;
import java.util.Random;
import java.util.UUID;

import me.razor.middleage.Config;
import me.razor.middleage.Main;
import me.razor.middleage.game.GameState;
import me.razor.middleage.game.Map;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;

public class VoteHandler {
	
	private Map map = null;
	private boolean allow_voting = true;

	private HashMap<Map , Integer> votes = new HashMap<Map, Integer>();
	public ArrayList<UUID> player_voted = new ArrayList<>();
	
	public void vote(Map m){
		if(!allow_voting){
			return;
		}
		if(!votes.containsKey(m)){
			return;
		}
		votes.put(m,(votes.containsKey(m) ? votes.get(m)+1 : 0));
		Main.getVoteHandler().updateScoreboard();
	}
	public int getVotes(Map check){
		if(check == null || !votes.containsKey(check)){
			return -1;
		}
		return votes.get(check);
	}
	public boolean hasVoted(Player p){
		return player_voted.contains(p.getUniqueId());
	}
	public void endVote(){
		if(!allow_voting){
			return;
		}
		allow_voting = false;
		int hightesVote = 0;
		for(Entry<Map,Integer> a : votes.entrySet()){
			if(a.getValue() > hightesVote){
				hightesVote = a.getValue();
			}
		}
		System.out.println(hightesVote);
		ArrayList<Map> winners = new ArrayList<>();
		for(Entry<Map, Integer> temp : votes.entrySet()){
			if(temp.getValue() == hightesVote){
				winners.add(temp.getKey());
			}
		}
		int randomIndex = new Random().nextInt(winners.size());
		map = winners.get(randomIndex);
	}
	
	public Map getMap(){
		return ((map == null || allow_voting)) ? null : map;
	}
	
	public void updateScoreboard(){
		if(!GameState.isState(GameState.LOBBY)){
			return;
		}
		Scoreboard s = Bukkit.getScoreboardManager().getNewScoreboard();
		Objective vote = s.registerNewObjective("vote","dummy");
		vote.setDisplaySlot(DisplaySlot.SIDEBAR);
		vote.setDisplayName(Config.Strings.SCOREBOARD);
		MapHandler handler = Main.getMapHandler();
		for(int i = 0 ; i < handler.getMaps().size(); i++){
			int score = Main.getVoteHandler().getVotes(handler.getMaps().get(i));
			if(score == -1){
				votes.put(handler.getMaps().get(i),0);
			}
			Score map = vote.getScore(handler.getMaps().get(i).getName().replaceAll("&","�"));
			map.setScore(Main.getVoteHandler().getVotes(handler.getMaps().get(i)));
		}
		for(Player p : Main.getGameHandler().all){
			p.setScoreboard(s);
		}
	}
	public boolean isVotingEnd(){
		return !allow_voting;
	}
	
}
