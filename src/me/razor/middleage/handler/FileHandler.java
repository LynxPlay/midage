package me.razor.middleage.handler;

import java.io.File;
import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.lynxplay.api.ConfigHandler;

import com.google.common.io.Files;

import me.razor.middleage.Config;
import me.razor.middleage.Main;
import me.razor.middleage.game.Quest;

public class FileHandler {

	public File getConfig(){
		return new File(Main.getPlugin().getDataFolder(),"config.yml");
	}
	public File getData(){
		return new File(Main.getPlugin().getDataFolder(),"data.yml");
	}
	public File getQuests(){
		return new File(Main.getPlugin().getDataFolder() , "quests.yml");
	}
	public File getWorldsFolder(){
		return new File(Main.getPlugin().getDataFolder() , "maps");
	}
	public File getTeamFile(){
		return new File(Main.getPlugin().getDataFolder() , "teams.yml");
	}
	public void setupDefaultConfig(){
		FileConfiguration c = YamlConfiguration.loadConfiguration(getConfig());
		c.options().copyDefaults(true);
		c.options().header("Hier koennt ihr Waerte veraendern");
		
		c.addDefault("prefix","&8&l[&R&EMidAge&8&l]");
		c.addDefault("error","&4Es gab einen Fehler. Bitte melde diesen Fehler");
		c.addDefault("no_permissions_msg","&4Du hast nicht genuegend Rechte daf�r");
		
		c.addDefault("lobby_time_left","&7Noch &6%time% &7verbleiben");
		c.addDefault("player_join","&6%player% &7ist beigetreten");
		c.addDefault("player_leave","&6%player% &7hat das Spiel verlassen");
		c.addDefault("kick_on_warmup","&4Leider startet das Spiel gerade");
		c.addDefault("kick_on_reset","&4Leider wird gerade neu gestartet");
		c.addDefault("not_enough_players","&CEs sind zu wenig Spieler online. Timer wird neu gestartet");
		c.addDefault("server_full","&4Der Server ist voll");
		c.addDefault("quest_choose","&7Es wird die Quest &6%quest%&7 gespielt");
		
		c.addDefault("warmup_time","&ANoch &6%time% &Averbleiben");
		
		c.addDefault("team_inv","&7[&ETeam-Auswahl&7]");
		c.addDefault("team_item","&6Team-Auswahl");
		c.addDefault("team_full","&6Das Team %team% ist voll");
		c.addDefault("no_map_votet","&4Momentan ist noch keine Map ausgew�hlt");
		c.addDefault("team_join","&7Du bist nun im Team &6%team%");
		c.addDefault("get_team","&ADu bist momentan in Team %team%");
		c.addDefault("teams.rot.data",14);
		c.addDefault("teams.blau.data",11);
		c.addDefault("teams.blau.name","&BBlau");
		c.addDefault("teams.rot.name","&4Rot");
		c.addDefault("teams.rot.color","FF0000");
		c.addDefault("teams.blau.color","0000FF");
		
		c.addDefault("vote_inv","&7[&6Vote&7]");
		c.addDefault("vote_item","&6Vote");
		c.addDefault("voting_end","&6Das Voting wurde beendet &7|| &6%map%");
		c.addDefault("player_voted","&7Du hast f�r die Map &6%map% &7gevotet");
		c.addDefault("has_voted","&4Du hast schon gevoted");
		c.addDefault("scoreboard_name","&6Voting");
		
		c.addDefault("gamestart","&6Das Spiel wurde gestartet");
		c.addDefault("player_death_killer","&6%player% &7wurde von %6%killer% &7get�tet");
		c.addDefault("player_death","&6%player% &7ist gestorben");
		c.addDefault("team_win","&6&lDas Team %team%&6&l hat gewonnen");
		
		c.addDefault("lobbytime", 180);
		c.addDefault("min_players",4);
		c.addDefault("max_players",32);
		c.addDefault("team_size",4);
		
		c.addDefault("scoreboard_ingame","&7<&l<&6%quest%&7&l>&R&7>");
		
		c.addDefault("download.Erde","Hier URL hin");
		
		try {
			c.save(getConfig());
		} catch (IOException e) {
			Main.getConsole().sendMessage("�4Die Datein konnten nicht gespeichert werden");
		}
	}
	public void setupDefaultData(){
		FileConfiguration c = YamlConfiguration.loadConfiguration(getData());
		c.options().copyDefaults(true);
		c.options().header("Hier koennt ihr Waerte veraendern");
		
		ConfigHandler.setLocation("lobby",new Location(Bukkit.getWorld("world"), 0, 0, 0),getData());
		c.addDefault("maps.Himmel.item","QUARTZ_BLOCK");
		c.addDefault("maps.Erde.item","DIRT");
		c.addDefault("maps.Nether.item","NETHERRACK");
		c.addDefault("maps.Himmel.name","&1Himmel");
		c.addDefault("maps.Erde.name","&BErde");
		c.addDefault("maps.Nether.name","&4Nether");
		
		try {
			c.save(getData());
		} catch (IOException e) {
			Main.getConsole().sendMessage("�4Die Datein konnten nicht gespeichert werden");
		}
	}
	public void setupQuests(){
		FileConfiguration c = YamlConfiguration.loadConfiguration(getQuests());
		c.options().copyDefaults(true);
		c.options().header("Quest Values");
		
		c.addDefault("steal.time_between_block_spawn",30);
		c.addDefault("steal.block_to_win",15);
		c.addDefault("steal.block_bringback","&7Der Block des Teams %team% &7ist wieder sicher");
		c.addDefault("steal.block_placed","&7Das Team %team% &7hat eine weiteren GoldBlock");
		c.addDefault("steal.block_steal","&7Der Block des Teams %team% &7wurde geklaut");
		c.addDefault("steal.two_blocks","&4Du darfst nur einen Block tragen");
		c.addDefault("steal.need_own_block","&7Dein Block darf nicht fehlen");
		c.addDefault("steal.has_flag","&6Du hast den Block des Teams %team%");
		c.addDefault("steal.blocktype","GOLD_BLOCK");
		
		try {
			c.save(getQuests());
		} catch (IOException e) {
			
		}
	}
	public void readConfig(){
		FileConfiguration c = YamlConfiguration.loadConfiguration(getConfig());
		
		Main.getChat().setPrefix(c.getString("prefix").replaceAll("&","�"));
		Config.Strings.ERROR = c.getString("error").replaceAll("&","�");
		Config.Strings.NO_PERMS = c.getString("no_permissions_msg").replaceAll("&","�");
		
		Config.Strings.LOBBY_TIME_LEFT = c.getString("lobby_time_left").replaceAll("&","�");
		Config.Strings.KICK_WARMUP = c.getString("kick_on_warmup").replaceAll("&","�");
		Config.Strings.KICK_RESET = c.getString("kick_on_reset").replaceAll("&","�");
		Config.Strings.PLAYER_JOIN = c.getString("player_join").replaceAll("&","�");
		Config.Strings.PLAYER_LEAVE = c.getString("player_leave").replaceAll("&","�");
		Config.Strings.NOT_ENOUGH_PLAYER = c.getString("not_enough_players").replaceAll("&","�");
		Config.Strings.WARMUP_TIME_LEFT = c.getString("warmup_time").replaceAll("&","�");
		Config.Strings.SERVER_FULL = c.getString("server_full").replaceAll("&","�");
		Config.Strings.QUEST_CHOOSE = c.getString("quest_choose").replaceAll("&","�");
		
		Config.Strings.TEAM_INV = c.getString("inv_name").replaceAll("&","�");
		Config.Strings.TEAM_ITEM = c.getString("team_item").replaceAll("&", "�");
		Config.Strings.TEAM_FULL = c.getString("team_full").replaceAll("&","�");
		Config.Strings.NO_MAP_VOTET = c.getString("no_map_votet").replaceAll("&","�");
		Config.Strings.GET_TEAM = c.getString("get_team").replaceAll("&","�");
		Config.Strings.TEAM_JOIN = c.getString("team_join").replaceAll("&","�");
		
		Config.Strings.VOTE_ITEM = c.getString("vote_item").replaceAll("&","�");
		Config.Strings.VOTE_INV = c.getString("vote_inv").replaceAll("&","�");
		Config.Strings.VOTING_END = c.getString("voting_end").replaceAll("&","�");
		Config.Strings.PLAYER_VOTED = c.getString("player_voted").replaceAll("&","�");
		Config.Strings.HAS_VOTED = c.getString("has_voted").replaceAll("&","�");
		Config.Strings.SCOREBOARD = c.getString("scoreboard_name").replaceAll("&", "�");
		
		Config.Strings.GAME_START = c.getString("gamestart").replaceAll("&", "�");
		Config.Strings.PLAYER_DEATH = c.getString("player_death").replaceAll("&","�");
		Config.Strings.PLAYER_DEATH_KILLER = c.getString("player_death_killer").replaceAll("&","�");
		
		Config.Values.LOBBY_TIME = c.getInt("lobbytime");
		Config.Values.MIN_PLAYER = c.getInt("min_players");
		Config.Values.MAX_PLAYER = c.getInt("max_players");
		Config.Values.TEAM_SIZE = c.getInt("team_size");
		
		Config.Strings.SCOREBOARD_INGAME = c.getString("scoreboard_ingame").replaceAll("&","�");
	}
	
	public void readData(){
		Config.Locs.LOBBY = ConfigHandler.getLocation("lobby", getData());
	}
	public void readQuest(){
		FileConfiguration c = YamlConfiguration.loadConfiguration(getQuests());
		
		Quest.Config.STEAL.REGENERATE = c.getInt("steal.time_between_block_spawn");
		Quest.Config.STEAL.BLOCK_TO_WIN = c.getInt("steal.block_to_win");
		Quest.Config.STEAL.BLOCK_REBUILD = c.getString("steal.block_bringback").replaceAll("&","�");
		Quest.Config.STEAL.BLOCK_PLACED = c.getString("steal.block_placed").replaceAll("&","�");
		Quest.Config.STEAL.BLOCK_STEAL = c.getString("steal.block_steal").replaceAll("&","�");
		Quest.Config.STEAL.TWO_BLOCKS = c.getString("steal.two_blocks").replaceAll("&", "�");
		Quest.Config.STEAL.NEED_OWN_BLOCK = c.getString("steal.need_own_block").replaceAll("&", "�");
		Quest.Config.STEAL.HAS_FLAG = c.getString("steal.has_flag").replaceAll("&", "�");
		Quest.Config.STEAL.BLOCK_TYPE = Material.valueOf(c.getString("steal.blocktype"));
		
	}
	public void createAllFiles(){
		File config = getConfig();
		File data = getData();
		File maps = getWorldsFolder();
		File quest = getQuests();
		File teams = getTeamFile();
		if(!maps.exists()){
			maps.mkdirs();
		}
		if(!teams.exists()){
			try {
				teams.createNewFile();
			} catch (IOException e) {
				
			}
			getFromMySQL();
		}
		if(!config.exists()){
			try {
				config.createNewFile();
			} catch (IOException e) {
				
			}
			getFromMySQL();
		}
		if(!data.exists()){
			try {
				data.createNewFile();
			} catch (IOException e) {
				
			}
			getFromMySQL();
		}
		if(!quest.exists()){
			try {
				quest.createNewFile();
			} catch (IOException e) {
				
			}
		}
	}
	
	public void reload(){
		setupDefaultConfig();
		setupDefaultData();
		setupQuests();
		readConfig();
		readData();
		readQuest();
	}
	public void getFromMySQL(){
		try {
			File config = getConfig();
			File data = getData();
			File quest = getData();
			String a = Main.getMySQL().getValue("FileName","config.yml","Config");
			String b = Main.getMySQL().getValue("FileName","data.yml","Config");
			String c = Main.getMySQL().getValue("FileName","quests.yml","Config");
			try {
				Files.write(a.getBytes(),config);
			} catch (IOException e) {
				
			}
			try {
				Files.write(b.getBytes(),data);
			} catch (IOException e) {
				
			}
			Files.write(c.getBytes(),quest);
		} catch (Exception e) {
			return;
		}
	}
}
