package me.razor.middleage.handler;

import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import sun.misc.BASE64Encoder;

public class Downloader {

	public static InputStream download(String adresse) throws NoSuchAlgorithmException, KeyManagementException {
		TrustManager[] trustManager = new TrustManager[] {
				new X509TrustManager() {
					
					@Override
					public X509Certificate[] getAcceptedIssuers() {
						// TODO Auto-generated method stub
						return null;
					}
					
					@Override
					public void checkServerTrusted(X509Certificate[] chain, String authType)
							throws CertificateException {
						// TODO Auto-generated method stub
						
					}
					
					@Override
					public void checkClientTrusted(X509Certificate[] chain, String authType)
							throws CertificateException {
						// TODO Auto-generated method stub
						
					}
				}
		};
		
		SSLContext ssl = SSLContext.getInstance("SSL");
		ssl.init(null,trustManager,new SecureRandom());
		HttpsURLConnection.setDefaultSSLSocketFactory(ssl.getSocketFactory());
		
		HostnameVerifier hostVertifier = new HostnameVerifier() {
			
			@Override
			public boolean verify(String hostname, SSLSession session) {
				return true;
			}
		};
		HttpsURLConnection.setDefaultHostnameVerifier(hostVertifier);
		
		String username = "username";
		String passwort = "passwort";
		
		try {
			
			URL url = new URL(adresse);
			String auth = username + ":" + passwort;
			String authE = new BASE64Encoder().encode(auth.getBytes());
			
			URLConnection con = url.openConnection();
			con.setRequestProperty("Authorization", "Basic " + authE);
			InputStream in = con.getInputStream();
			
			return in;
			
		} catch (Exception e) {
			return null;
		}
	}
	
}
