package me.razor.middleage;

import org.bukkit.Location;

public class Config {

	public static class Strings  {
		
		//Server Strings
		public static String NO_PERMS = "";
		public static String ERROR = "";
		
		//LOBBY Thread
		public static String LOBBY_TIME_LEFT = "";
		public static String KICK_WARMUP = "";
		public static String KICK_RESET = "";
		public static String PLAYER_JOIN =	"";
		public static String PLAYER_LEAVE = ""; 
		public static String NOT_ENOUGH_PLAYER = "";
		public static String SERVER_FULL = "";
		public static String QUEST_CHOOSE = "";
		
		//WARMUP
		public static String WARMUP_TIME_LEFT = "";
		
		//INGAME
		public static String GAME_START = "";
		public static String PLAYER_DEATH = "";
		public static String PLAYER_DEATH_KILLER = "";
		public static String TEAM_WIN = "";
		
		//Team Strings
		public static String TEAM_INV = "";
		public static String TEAM_ITEM = "";
		public static String TEAM_FULL = "";
		public static String NO_MAP_VOTET  = "";
		public static String GET_TEAM = "";
		public static String TEAM_JOIN = "";
		
		//Vote Strings
		public static String VOTE_INV = "";
		public static String VOTE_ITEM = "";
		public static String VOTING_END = "";
		public static String PLAYER_VOTED = "";
		public static String HAS_VOTED = "";
		public static String SCOREBOARD = "";
		public static String SCOREBOARD_INGAME;
	}
	
	public static class Values {
		public static int LOBBY_TIME = 180;
		public static int WARMUP_TIME = 10;
		public static int INGAME_TIME = 10;
		public static int MIN_PLAYER = 4;
		public static int MAX_PLAYER = 32;
		public static int TEAM_SIZE = 4;
	}
	
	public static class Locs {
		public static Location LOBBY = null;
	}
}
